package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.endpoint.*;
import ru.konovalov.tm.model.Session;

public interface EndpointLocator {

    @NotNull

    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull IPropertyService getPropertyService();

    @Nullable Session getSession();

    void setSession(@Nullable Session session);

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndpoint();

}
