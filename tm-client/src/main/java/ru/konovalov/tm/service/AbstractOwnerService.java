package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.api.IOwnerService;
import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IAuthService;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractOwnerService<E extends AbstractOwner> extends AbstractService<E> implements IOwnerService<E> {

    protected final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    public List<E> findAll(final @NotNull String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    public List<E> findAll(final @NotNull String userId, final Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    public E findById(@NotNull final String userId, @NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public int size(@NotNull final String userId) {
        if (repository.size() == 0) return 0;
        return repository.size(userId);
    }

    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(userId, id);
        return null;
    }

}
