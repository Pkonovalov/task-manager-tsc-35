package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-c";
    }

    @NotNull
    @Override
    public final String name() {
        return "commands";
    }

    @NotNull
    @Override
    public final String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) System.out.println(command.name());
    }

}
